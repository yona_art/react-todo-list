package com.todo.list.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * CLItems
 */
@Entity
@Table(name = "CheckListItems")
public class CLItems {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String description;
    private int cheked;
    // @JsonIgnore
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "task_id", nullable = false)
    private Tasks Tasks;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCheked() {
        return cheked;
    }

    public void setCheked(int cheked) {
        this.cheked = cheked;
    }

    public Tasks getTasks() {
        return Tasks;
    }

    public void setTasks(Tasks tasks) {
        this.Tasks = tasks;
    }

}