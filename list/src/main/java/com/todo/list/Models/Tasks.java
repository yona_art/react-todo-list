package com.todo.list.Models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
/**
 * Tasks
 */
@Entity
@Table(name = "Tasks")
@JsonIdentityInfo(
  generator = ObjectIdGenerators.PropertyGenerator.class, 
  property = "id")
public class Tasks {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String description;
    private int status;
    private int haveCheck;
    private int finished;
    @JsonIgnore
    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "Tasks", fetch= FetchType.LAZY)
    private List<CLItems> CHItems = new ArrayList<CLItems>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHaveCheck() {
        return haveCheck;
    }

    public void setHaveCheck(int haveCheck) {
        this.haveCheck = haveCheck;
    }


    public List<CLItems> getCHItems() {
        return CHItems;
    }

    public void setCHItems(List<CLItems> cHItems) {
        this.CHItems = cHItems;
    }

    public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
    }
    
    public int getFinished() {
        return finished;
    }

    public void setFinished(int finished) {
        this.finished = finished;
    }
}