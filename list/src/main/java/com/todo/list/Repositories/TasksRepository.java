package com.todo.list.Repositories;

import java.util.List;

import com.todo.list.Models.Tasks;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * TasksRepository
 */
public interface TasksRepository extends JpaRepository<Tasks, Integer> {
    
    List<Tasks> findByFinished(int finished);
    
}