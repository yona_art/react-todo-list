package com.todo.list.Repositories;

import java.util.List;

import com.todo.list.Models.CLItems;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



/**
 * StatusRepository
 */
@Repository
public interface ClItemsRepository extends JpaRepository<CLItems, Integer> {
    
    List<CLItems> findAllById(int id);

    @Query("FROM CLItems g where g.Tasks.id = :task_id")
    List<CLItems> findByTasks(@Param("task_id")int id);

}