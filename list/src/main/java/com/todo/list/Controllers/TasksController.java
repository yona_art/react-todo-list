package com.todo.list.Controllers;

import java.util.List;

import com.todo.list.Models.CLItems;
import com.todo.list.Models.Tasks;
import com.todo.list.Services.CLItemsService;
import com.todo.list.Services.TasksService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TaksController
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("tasks")
public class TasksController {

    @Autowired
    private TasksService service;

    @Autowired
    private CLItemsService Itemsservice;

    @GetMapping(value = "/")
    public ResponseEntity<List<Tasks>> findAll() {
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Tasks> findOne(@PathVariable("id") int id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @GetMapping(value = "/{finished}/finished")
    public ResponseEntity<List<Tasks>> findAllByFinish(@PathVariable("finished") int id) {
        return new ResponseEntity<>(service.getByFinished(id), HttpStatus.OK);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Tasks> save(@RequestBody Tasks tasks) {
        return new ResponseEntity<>(service.save(tasks), HttpStatus.CREATED);
    }

    @PostMapping(value = "/{id}/items")
    public ResponseEntity<CLItems> saveItem(@RequestBody CLItems items, @PathVariable("id") int id) {
        Tasks item = service.findById(id);
        items.setTasks(item);
        Itemsservice.save(items);
        // .orElseThrow(() -> new ResourceNotFoundException("Card " + id + " not found"));
        return new ResponseEntity<>(
            items 
        , HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Tasks> update(@PathVariable("id") int id, @RequestBody Tasks tasks) {
        System.out.println(tasks.getFinished());
        return new ResponseEntity<>(service.update(id, tasks), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}/{finished}")
    public ResponseEntity<Tasks> updateFinished(@PathVariable("id") int id, @PathVariable("finished") int finished) {
        return new ResponseEntity<>(service.updateFinished(id, finished), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") int id) {
        service.delete(service.findById(id));
        return new ResponseEntity<>(HttpStatus.OK);
    }

}