package com.todo.list.Controllers;

import java.util.ArrayList;
import java.util.List;

import com.todo.list.Models.CLItems;
import com.todo.list.Services.CLItemsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * CLItemsController
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("items")
public class CLItemsController {

    @Autowired
    private CLItemsService service;

    @GetMapping(value = "/")
    public ResponseEntity<List<CLItems>> findAll() {
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<List<CLItems>> findAllById(@PathVariable("id") int id) {
        List<CLItems> items = service.findById(id);
        return new ResponseEntity<>(items, HttpStatus.OK);
    }

    @PostMapping(value = "/")
    public ResponseEntity<CLItems> save(@RequestBody CLItems item, @PathVariable("id") int id) {
        return new ResponseEntity<>(service.save(item), HttpStatus.CREATED);
    }

    @PostMapping(value = "/All")
    public ResponseEntity<List<CLItems>> saveAll(@RequestBody List<CLItems> containers, @PathVariable("id") int id) {
        List<CLItems> list = new ArrayList<>();
        containers.stream().forEach((c) -> {
            list.add(service.save(c));
        });
        return new ResponseEntity<>(list, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<CLItems> update(@PathVariable int id, @RequestBody CLItems container) {
        return new ResponseEntity<>(service.update(id, container), HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<List<CLItems>> updateAll(@RequestBody List<CLItems> containers) {
        List<CLItems> list = new ArrayList<>();
        containers.stream().forEach((c) -> {
            list.add(service.update(c.getId(), c));
        });
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable int id) {
        service.delete(service.getOne(id));
        return new ResponseEntity<>(HttpStatus.OK);
    }

}