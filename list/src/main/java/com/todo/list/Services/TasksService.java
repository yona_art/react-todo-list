package com.todo.list.Services;

import java.util.List;

import com.todo.list.Models.Tasks;
import com.todo.list.Repositories.TasksRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TasksService
 */
@Service
public class TasksService {

    @Autowired
    private TasksRepository repository;

    public Tasks save(Tasks taks) {
        taks.setFinished(0);
        return repository.save(taks);
    }

    public Tasks update(int id, Tasks taks) {
        Tasks OldTask = repository.getOne(id);  
        // OldTask.setDescription(taks.getDescription());
        OldTask.setStatus(taks.getStatus());
        OldTask.setHaveCheck(taks.getHaveCheck());
        OldTask.setFinished(taks.getFinished());
        return repository.save(OldTask);
    }

    public void delete(Tasks taks) {
        repository.delete(taks);
    }

    public Tasks findById(int id) {
        return repository.getOne(id);
    }

    public List<Tasks> getAll() {
        return repository.findAll();
    }

    public List<Tasks> getByFinished(int finished) {
        return repository.findByFinished(finished);
    }

    public Tasks updateFinished( int id, int finished){
        Tasks tasks = repository.findById(id).get();
        tasks.setFinished(finished);
        return repository.save(tasks);
    }
    
}