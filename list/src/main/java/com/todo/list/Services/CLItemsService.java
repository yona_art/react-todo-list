package com.todo.list.Services;

import java.util.List;

import com.todo.list.Models.CLItems;
import com.todo.list.Repositories.ClItemsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * CLItems
 */
@Service
public class CLItemsService {

    @Autowired
    private ClItemsRepository repository;


    public CLItems save(CLItems CLItems) {
        return repository.save(CLItems);
    }

    public CLItems update(int id, CLItems CLItems) {
        CLItems OldCLItems = repository.getOne(id);
        OldCLItems.setDescription(CLItems.getDescription());
        OldCLItems.setCheked(CLItems.getCheked());
        return repository.save(OldCLItems);
    }

    public void delete(CLItems CLItems) {
        repository.delete(CLItems);
    }

    public List<CLItems> findById(int id) {
        List<CLItems> items = repository.findByTasks(id);
        return items;
    }

    public CLItems getOne(int id) {
        return repository.getOne(id);
    }

    public List<CLItems> getAll() {
        return repository.findAll();
    }
}