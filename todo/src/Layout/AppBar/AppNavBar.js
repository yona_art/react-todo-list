import React, { Component } from 'react';
import './AppNavBar.css';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import AddDialogTask from '../../Components/AddDialog/index';
import Container from 'react-bootstrap/Container';
import Checkbox from '@material-ui/core/Checkbox';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class AppNavBar extends Component {

    state = {
        checked: false
    }

    constructor() {
        super();
        this.sendData = this.sendData.bind(this);
        this.sendRefresh = this.sendRefresh.bind(this);
    }

    sendData() {
        if (this.state.checked === true) {
            this.setState({ checked: false });
            this.props.parentCallback(1);
        } else {
            this.setState({ checked: true });
            this.props.parentCallback(0);
        }
    }

    sendRefresh() {
        this.props.refreshDataCallback(true);
    }

    refreshData = (childata) =>{
        if(childata){
          this.sendRefresh();
        }
      }
    


    render() {

        return (
            <div className="AppNavBar" >
                <AppBar position="static">
                    <Toolbar variant="dense">
                        <Container>
                            <Row>
                                <Col>
                                    <Typography variant="h6" color="inherit">
                                        React Todo List
                                    </Typography>
                                </Col>
                                <Col>
                                    <AddDialogTask refreshDataCallback={this.refreshData}  />

                                </Col>
                                <Col>
                                    <Checkbox
                                        checked={this.state.checked}
                                        onChange={this.sendData} />
                                    Terminados
                                </Col>
                            </Row>
                        </Container>
                    </Toolbar>
                </AppBar>
                <br></br>
            </div>
        );
    }
}

export default AppNavBar
