import React from 'react';
import './App.css';
import axios from 'axios';
import AppNavBar from './Layout/AppBar/AppNavBar';
import OutlinedCard from './Components/Card/index'
import LinearIndeterminate from './Components/ProgressBar/index'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default class App extends React.Component {

  state = {
    finished: 0,
    message: 0,
    cards: [],
  }

  constructor() {
    super();
    this.handleData = this.handleData.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  handleData = (childData) => {
    this.setState({ finished: childData });
    this.getData();
  }

  refreshData = (childata) =>{
    if(childata){
      this.getData();
    }
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    axios.get("http://localhost:8080/api/v1/tasks/" + this.state.finished + "/finished")
      .then(data => this.setState({ cards: data.data }))
      .catch(err => {
        console.log(err);
        return null;
      });
  }

  render() {
    return (
      <div className="App">
        <AppNavBar parentCallback={this.handleData}  refreshDataCallback={this.refreshData} ></AppNavBar>
        <Container>
          <Row>
            {this.state.cards.length === 0 ? (
              <Col >
                <LinearIndeterminate />
              </Col>

            ) : (
                this.state.cards.map((e, i) => {
                  return <div key={i}>
                    <Col >
                      <OutlinedCard conf={e}></OutlinedCard>
                    </Col>
                  </div>;
                })
              )}
          </Row>
        </Container>
      </div>
    );
  }

}
