import React from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneIcon from '@material-ui/icons/Done';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ScrollDialog from '../ListDialog/index';

export default class OutlinedCard extends React.Component {
    state = {
        classes: {},
        id: 0,
        description: '',
        status: 0,
        haveCheck: 0,
    }

    componentDidMount() {
        this.setState({
            classes: makeStyles({
                root: {
                    minWidth: 275,
                }
            })
        });
    }


    updateFinished = () => {
        let config = this.props.conf;
        config.finished === 0 ? config.finished = 1 : config.finished = 0;
        axios.put(("http://localhost:8080/api/v1/tasks/" + config.id + "/" + config.finished))
            .then(data => this.setState({ cards: data.data }))
            .catch(err => {
                console.log(err);
                return null;
            });
    }


    deleteTasks() {
        axios.delete(("http://localhost:8080/api/v1/tasks/"+ this.props.conf.id))
            .then(data => this.setState({ cards: data.data }))
            .catch(err => {
                console.log(err);
                return null;
            });
    }

    render() {
        return (
            <Card className={this.state.classes.root} variant="outlined">
                <CardContent>
                    <Typography variant="h5" component="h2">
                        {this.props.conf.description}
                    </Typography>
                    <Container>
                        <Row>
                            <Col lg sm={10}>
                                {this.props.conf.chitems.lenght > 0 ? (
                                    <Typography variant="body2" component="p">

                                        Agrega una lista de chequeo
                                    </Typography>
                                ) : (
                                        <Typography variant="body2" component="p">
                                            Revisa tu lista de chequeo
                                    </Typography>
                                    )}
                            </Col>
                            <Col>
                                <ScrollDialog id={this.props.conf.id} conf={this.props.conf} />
                            </Col>
                        </Row>
                    </Container>
                </CardContent>
                <CardActions>
                    <Button variant="contained" color="primary" onClick={this.updateFinished}>
                        {this.props.conf.finished === 0 ? "Terminar" : "Reusar"}
                        <DoneIcon></DoneIcon>
                    </Button>
                    <Button variant="contained" color="secondary" onClick={this.deleteTasks}>
                        Eliminar<DeleteIcon></DeleteIcon>
                    </Button>
                </CardActions>
            </Card>
        );
    }
}



