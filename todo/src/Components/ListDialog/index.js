import React from 'react';
import axios from 'axios';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import ReorderIcon from '@material-ui/icons/Reorder';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Chip from '@material-ui/core/Chip';
import CheckboxList from './List/Index';
import LinearIndeterminate from '../ProgressBar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ItemDialog from '../AddItemDialog';


export default function ScrollDialog(props) {

    const [open, setOpen] = React.useState(false);
    const [scroll, setScroll] = React.useState('paper');
    const [items, setItems] = React.useState([]);

    const handleClickOpen = scrollType => () => {
        setOpen(true);
        setScroll(scrollType);
        axios.get(("http://localhost:8080/api/v1/items/" + props.id))
            .then(data => setItems(data.data))
            .catch(err => {
                return null;
            });
    };

    const handleClose = () => {
        setOpen(false);
    };

    const descriptionElementRef = React.useRef(null);
    React.useEffect(() => {
        if (open) {
            const { current: descriptionElement } = descriptionElementRef;
            if (descriptionElement !== null) {
                descriptionElement.focus();
            }
        }
    }, [open]);


    return (
        <div>
            <Chip icon={<ReorderIcon />} color="primary" onClick={handleClickOpen('body')} label="Abrir" />
            <Dialog
                open={open}
                onClose={handleClose}
                scroll={scroll}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogTitle id="scroll-dialog-title">
                    <Container>
                        <Row>
                            <Col >
                                Check List
                                </Col>
                            <Col>
                                <ItemDialog  conf={props.conf} />
                            </Col>
                        </Row>
                    </Container>
                </DialogTitle>
                <DialogContent dividers={scroll === 'body'}>
                    <DialogContentText
                        id="scroll-dialog-description"
                        ref={descriptionElementRef}
                        tabIndex={-1}>
                    </DialogContentText>
                    <Container>
                        <Row>
                            {items.length === 0 ? (
                                <Col>
                                    <LinearIndeterminate />

                                </Col>
                            ) : (
                                    <Col>
                                        <CheckboxList items={items} />
                                    </Col>
                            )}
                        </Row>
                    </Container>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cerrar<CloseIcon />
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}