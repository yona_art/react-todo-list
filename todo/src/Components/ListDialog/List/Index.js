import React from 'react';
import axios from 'axios';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import DeleteIcon from '@material-ui/icons/Delete';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default class CheckboxList extends React.Component {

  state = {
    items: []
  }


  componentDidMount() {
    this.setState({ items: this.props.items });
  }

  onAddingItem = (i) => async (event) => {
    await this.setState((state, props) => {
      state.items[i].cheked === 0 ? state.items[i].cheked = 1 : state.items[i].cheked = 0;
      return {
        items: state.items
      }
    });
    this.updatedChecked(this.state.items[i]);
  }

  updatedChecked(item) {
    console.log(item);
    axios.put(("http://localhost:8080/api/v1/items/" + item.id), item)
    .catch(err => {
        return null;
    });
  }


  deleteItem(product, i) {
    this.state.items.splice(i, 1);
    this.setState({ items: this.state.items });
    this.deleteRequest(product);
  }

  deleteRequest(product) {
    axios.delete(("http://localhost:8080/api/v1/items/" + product.id))
      .then(data => this.setState({ cards: data.data }))
      .catch(err => {
        console.log(err);
        return null;
      });
  }

  render() {
    const items = this.state.items.map((product, i) => {
      return (
        <tr key={product.id}>
          <td>
            <Container>
              <Row>
                <Col>
                  <Checkbox
                    value={product.description}
                    checked={product.cheked !== 0}
                    onChange={this.onAddingItem(i)} />
                  {product.description}
                </Col>
                <Col>
                  <Chip icon={<DeleteIcon />} color="secondary" onClick={() => { this.deleteItem(product, i) }} label="Eliminar" />
                </Col>
              </Row>
            </Container>
          </td>
        </tr>
      )
    })
    return (
      <div>
        <table>
          <tbody>
            {items}
          </tbody>
        </table>
      </div>
    );
  }

}