import React from 'react';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';

export default function FormDialog(props) {
    const [data, setData] = React.useState({});
    const [open, setOpen] = React.useState(false);
    let [text, setText] = React.useState('');
    let [number, setNumber] = React.useState('');

    const [state, setState] = React.useState({
        opens: false,
        vertical: 'top',
        horizontal: 'center',
    });

    const { vertical, horizontal, opens } = state;

    const snackHandleClick = newState => () => {
        setState({ open: true, ...newState });
    };

    const snackHandleClose = () => {
        setState({ ...state, open: false });
    };


    const handleClickOpen = () => {
        setOpen(true);
        props.refreshDataCallback(true);
        if (text !== '' && number !== 0) {
            saveData();
            if (data.id !== 0) {
                snackHandleClick({ vertical: 'bottom', horizontal: 'center' });
                setOpen(false);
            }
        }
    };

    const handleClose = () => {
        setOpen(false);

    };
    const onclick = () => {
        setOpen(true);

    };

    const saveData = () => {
        axios.post("http://localhost:8080/api/v1/tasks/", {
            description: text,
            haveCheck: 0,
            status: number,
        })
            .then(data => setData({ data: data.data }))
            .catch(err => {
                console.log(err);
                return null;
            });
    }



    return (
        <div>
            <Button variant="contained" color="secondary"
                onClick={onclick}>
                Agregar<AddIcon></AddIcon>
            </Button>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Subscribe</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Rellene los campos para crear una  nueva tarea
                        </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Titulo"
                        type="text"
                        fullWidth
                        value={text}
                        onChange={(e) => setText(e.target.value)}
                    />
                    <TextField
                        id="number"
                        label="Prioridad"
                        type="number"
                        margin="dense"
                        InputLabelProps={{
                            shrink: true,
                            min: 0,
                            max: 10
                        }}
                        value={number}
                        onChange={(e) => setNumber(e.target.value)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cerrar <CloseIcon />
                    </Button>
                    <Button onClick={handleClickOpen} color="primary">
                        Agregar Tarea <AddIcon />
                    </Button>
                </DialogActions>
            </Dialog>

            <Snackbar
                anchorOrigin={{ vertical, horizontal }}
                key={`${vertical},${horizontal}`}
                open={opens}
                onClose={snackHandleClose}
                message="Elemento Creado"
            />
        </div>
    )
}

